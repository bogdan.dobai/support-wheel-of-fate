import {combineReducers} from 'redux';

import AppState from '../state/AppState';
import engineersReducer from '@engineers/store/reducer';

export default combineReducers<AppState>({
  engineers: engineersReducer,
});
