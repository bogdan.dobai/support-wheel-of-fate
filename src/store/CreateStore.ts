import {applyMiddleware, createStore, compose} from 'redux';
import appReducer from './reducers';
import * as Types from './state';
import {createLogger} from 'redux-logger';

export default () => {
  const composeEnhancers =
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const middlewares = [];
  if (process.env.NODE_ENV === 'development') {
    const logger = createLogger({
      level: 'info',
      collapsed: true,
      timestamp: false,
      duration: true,
    });
    middlewares.push(logger);
  }

  const store = createStore<Types.RootState, Types.RootAction, any, any>(
    appReducer,
    composeEnhancers(applyMiddleware(...middlewares)),
  );
  return {store};
};
