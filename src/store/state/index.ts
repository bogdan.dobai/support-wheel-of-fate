import {StateType} from 'typesafe-actions';
import rootReducer from '../reducers';
import {EngineersActions} from '@engineers/store/reducer';

export type RootAction = EngineersActions;

export type RootState = StateType<typeof rootReducer>;
