import {EngineersState} from '@engineers/store/reducer';

export default interface AppState {
  engineers: EngineersState;
}
