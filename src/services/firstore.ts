import firestore from '@react-native-firebase/firestore';
import {EngineerClass} from '@engineers/types/classes';

export async function getEngineers() {
  const engineers = [];
  const snapshot = await firestore().collection('engineers').get();
  snapshot.forEach(engineer => {
    engineers.push(engineer.data());
  });
  return engineers;
}

export async function addEngineer(
  firstName: string,
  lastName: string,
  department: string,
  position: string,
) {
  try {
    const docRef = await firestore()
      .collection('engineers')
      .add({firstName, lastName, department, position});
    await firestore().collection('engineers').doc(docRef.id).update({
      id: docRef.id,
    });
    return {
      id: docRef.id,
      firstName,
      lastName,
      department,
      position,
    } as EngineerClass;
  } catch (e) {}
}

export async function deleteEngineer(id: string) {
  try {
    await firestore().collection('engineers').doc(id).delete();
  } catch (e) {}
}

export async function editEngineer(
  id: string,
  firstName: string,
  lastName: string,
  department: string,
  position: string,
) {
  try {
    await firestore().collection('engineers').doc(id).update({
      firstName,
      lastName,
      department,
      position,
    });
    return {
      id,
      firstName,
      lastName,
      department,
      position,
    } as EngineerClass;
  } catch (e) {}
}
