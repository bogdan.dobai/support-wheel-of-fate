import storage from '@react-native-firebase/storage';

/**
 * Not the right way of getting an image from firebase, but as long as the storage is public(for testing) this works.
 *
 * @param fileName
 */
export const getImage = (fileName: string, overrideCache?: boolean) => {
  if (!fileName) {
    return;
  }
  return `https://firebasestorage.googleapis.com/v0/b/support-wheel-of-fate-74dee.appspot.com/o/profile-pictures%2F${fileName}?alt=media${
    overrideCache ? `#${new Date().getTime()}` : ''
  }`;
};

export const uploadImage = (path: string, image) => {
  try {
    const reference = storage().ref(`profile-pictures/${path}`);
    return reference.putFile(image);
  } catch (e) {}
};
