export const Strings = {
  NOTHING_TO_SHOW: 'Nothing to show',
  ADD_ENGINEERS: 'Add Engineers',
  DEPARTMENT: 'Department',
  FIRST_NAME: 'First Name',
  LAST_NAME: 'Last Name',
  POSITION: 'Position',
  SAVE: 'Save Changes',
};
