export enum MainNavigatorRoutes {
  HOME = 'Home',
  ENGINEER_DETAILS = 'Engineer Details',
}

export type MainNavigatorRouteProps = {
  [MainNavigatorRoutes.HOME]: undefined;
  [MainNavigatorRoutes.ENGINEER_DETAILS]: {editable: boolean};
};
