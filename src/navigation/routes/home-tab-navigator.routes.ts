export enum HomeTabNavigatorRoutes {
  ENGINEERS = 'Engineers',
  SHIFTS = 'Shifts',
  ENGINEER_DETAILS = 'Engineer Details',
}

export type HomeTabNavigatorRouteProps = {
  [HomeTabNavigatorRoutes.ENGINEERS]: undefined;
  [HomeTabNavigatorRoutes.ENGINEER_DETAILS]: {editable: boolean};
  [HomeTabNavigatorRoutes.SHIFTS]: undefined;
};
