import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {MainNavigatorRouteProps, MainNavigatorRoutes} from '../routes';
import {HomeTabNavigator} from './home-tab-navigator';
import {EngineerDetails} from '@src/modules/engineers/screens';
import {HeaderBackButton} from '@src/components';
import {Metrics} from '@src/theme';
import {OverlayHeaderBackground} from '@src/modules/engineers/components';

const Stack = createStackNavigator<MainNavigatorRouteProps>();

export const MainNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{title: ''}}>
      <Stack.Screen
        name={MainNavigatorRoutes.HOME}
        component={HomeTabNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={MainNavigatorRoutes.ENGINEER_DETAILS}
        component={EngineerDetails}
        options={({navigation}) => ({
          headerMode: 'float',
          headerTransparent: true,
          headerBackground: () => <OverlayHeaderBackground />,
          headerLeftContainerStyle: {
            paddingLeft: Metrics.horizontal._24,
          },
          headerRightContainerStyle: {paddingRight: Metrics.horizontal._24},
          headerLeft: () => (
            <HeaderBackButton color={'white'} onPress={navigation.goBack} />
          ),
        })}
      />
    </Stack.Navigator>
  );
};
