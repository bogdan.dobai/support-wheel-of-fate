import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeTabNavigatorRouteProps, HomeTabNavigatorRoutes} from '../routes';
import {Engineers} from '@src/modules/engineers/screens';
import {CalendarIcon, ListIcon} from '@src/assets/icons';
import {ICON_SIZE} from '@src/helpers';
import {Colors, Metrics} from '@src/theme';
import {Shifts} from '@src/modules/shifts/screens';

const Tab = createBottomTabNavigator<HomeTabNavigatorRouteProps>();

export const HomeTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name={HomeTabNavigatorRoutes.ENGINEERS}
        component={Engineers}
        options={{
          headerTitleAlign: 'center',
          tabBarLabel: '',
          tabBarIcon: ({focused}) => (
            <ListIcon
              style={{marginTop: Metrics.vertical._15}}
              width={ICON_SIZE}
              height={ICON_SIZE}
              fill={focused ? Colors.primary : Colors.disabled}
            />
          ),
          headerRightContainerStyle: {paddingRight: Metrics.horizontal._24},
        }}
      />
      <Tab.Screen
        name={HomeTabNavigatorRoutes.SHIFTS}
        component={Shifts}
        options={{
          headerTitleAlign: 'center',
          tabBarLabel: '',
          tabBarIcon: ({focused}) => (
            <CalendarIcon
              style={{marginTop: Metrics.vertical._15}}
              width={ICON_SIZE}
              height={ICON_SIZE}
              fill={focused ? Colors.primary : Colors.disabled}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
