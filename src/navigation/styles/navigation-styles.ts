import {Metrics} from '@src/theme';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  bottomIcon: {
    marginTop: Metrics.vertical._12,
    paddingBottom: 0,
  },
});
