import * as React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {RootContainer} from './root-container/root-container';
import createStore from '@src/store/CreateStore';
import {Provider} from 'react-redux';

const {store} = createStore();

export const App = () => (
  <Provider store={store}>
    <SafeAreaProvider>
      <RootContainer />
    </SafeAreaProvider>
  </Provider>
);
