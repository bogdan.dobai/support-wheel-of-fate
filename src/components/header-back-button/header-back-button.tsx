import {Colors} from '@src/theme';
import * as React from 'react';
import {Pressable} from 'react-native';
import {ArrowLeftIcon} from '@src/assets/icons';
import {ICON_SIZE} from '@src/helpers';

interface Props {
  onPress: () => void;
  color?: string;
}

export const HeaderBackButton = (props: Props) => (
  <Pressable onPress={props.onPress} style={{zIndex: 5, elevation: 5}}>
    <ArrowLeftIcon
      fill={props.color || Colors.primary}
      width={ICON_SIZE}
      height={ICON_SIZE}
    />
  </Pressable>
);
