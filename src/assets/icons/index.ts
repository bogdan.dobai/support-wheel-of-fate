import ArrowRightIcon from './arrow_right.svg';
import ArrowLeftIcon from './arrow_left.svg';
import CalendarIcon from './calendar.svg';
import ListIcon from './list.svg';
import MenuDotsIcon from './menu_dots.svg';
import SearchIcon from './search.svg';
import SaveIcon from './save.svg';
import CloseIcon from './close.svg';
import TrashIcon from './trash.svg';
import EditIcon from './edit.svg';
import AddIcon from './add.svg';
import UploadIcon from './upload.svg';

export {
  ArrowRightIcon,
  ArrowLeftIcon,
  CalendarIcon,
  ListIcon,
  MenuDotsIcon,
  SearchIcon,
  SaveIcon,
  CloseIcon,
  TrashIcon,
  EditIcon,
  AddIcon,
  UploadIcon,
};
