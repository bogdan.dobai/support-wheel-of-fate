import {useEffect, useState} from 'react';
import {InteractionManager} from 'react-native';

/**
 * Get the data from the store or load it otherwise
 * Returns the loading
 *
 * @param getResources -> function that return the backend response
 * @param resources -> current value in store
 * @param onSuccess -> function that can map the results before setting it in the resources
 * @param onFail -> function that can map the results before setting it in the resources
 */

interface Props<T> {
  getResources: () => Promise<T[]>;
  resources: T[];
  onSuccess: (resources: T[]) => void;
  onFail?: (error: string) => void;
}

export const useStoreLoad = <T>({
  getResources,
  resources,
  onSuccess,
  onFail,
}: Props<T>) => {
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    InteractionManager.runAfterInteractions(async () => {
      try {
        if (resources.length) return;
        setLoading(true);
        const data: T[] = await getResources();
        onSuccess(data);
        setLoading(false);
      } catch (e) {
        onFail && onFail(e.message);
        setLoading(false);
      }
    });
  }, []);

  return loading;
};
