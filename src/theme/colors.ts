export const Colors = {
  primary: '#101820',
  accent: '#F7BF6F',
  background: '#F4F4F4',
  white: '#FFFFFF',
  disabled: '#C1CFD6',
};
