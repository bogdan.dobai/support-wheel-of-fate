export const Fonts = {
  black: 'NunitoSans-Black',
  bold: 'NunitoSans-Bold',
  regular: 'NunitoSans-Regular',
  semiBold: 'NunitoSans-SemiBold',
  light: 'NunitoSans-Light',
};
