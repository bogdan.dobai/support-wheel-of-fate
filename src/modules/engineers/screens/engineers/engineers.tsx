import * as React from 'react';
import {View, StyleSheet, Pressable} from 'react-native';
import {EngineerClass} from '@src/modules/engineers/types/classes';
import {EngineerList} from '@src/modules/engineers/components';
import {Colors, Metrics} from '@src/theme';
import {StackScreenProps} from '@react-navigation/stack';
import {
  HomeTabNavigatorRouteProps,
  HomeTabNavigatorRoutes,
} from '@src/navigation/routes';
import {useStoreLoad} from '@src/hooks';
import {getEngineers} from '@src/services/firstore';
import {useDispatch, useSelector} from 'react-redux';
import {selectEngineers} from '@engineers/store/selectors';
import {LoadEngineers, SelectEngineer} from '@engineers/store/actions';
import {useEffect} from 'react';
import {AddIcon} from '@src/assets/icons';
import {ICON_SIZE} from '@src/helpers';

export const Engineers = (
  props: StackScreenProps<
    HomeTabNavigatorRouteProps,
    HomeTabNavigatorRoutes.ENGINEERS
  >,
) => {
  const dispatch = useDispatch();
  const engineers = useSelector(selectEngineers);

  const onAddEngineer = () => {
    dispatch(SelectEngineer(null));
    props.navigation.navigate(HomeTabNavigatorRoutes.ENGINEER_DETAILS, {
      editable: true,
    });
  };

  useEffect(() => {
    props.navigation.setOptions({
      headerRight: () => (
        <Pressable onPress={onAddEngineer}>
          <AddIcon width={ICON_SIZE} height={ICON_SIZE} fill={Colors.primary} />
        </Pressable>
      ),
    });
  }, []);

  const onSelectEngineer = (item: EngineerClass) => {
    dispatch(SelectEngineer(item.id));
    props.navigation.navigate(HomeTabNavigatorRoutes.ENGINEER_DETAILS);
  };

  const onAddEngineers = () => {
    props.navigation.navigate(HomeTabNavigatorRoutes.ENGINEER_DETAILS, {
      editable: true,
    });
  };

  const onLoadEngineers = (data: EngineerClass[]) => {
    dispatch(LoadEngineers(data));
  };

  const loading = useStoreLoad({
    resources: engineers,
    getResources: getEngineers,
    onSuccess: onLoadEngineers,
  });

  return (
    <View style={styles.container}>
      <EngineerList
        data={engineers}
        loading={loading}
        onSelectEngineer={onSelectEngineer}
        onAddEngineers={onAddEngineers}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
    paddingTop: Metrics.horizontal._24,
  },
});
