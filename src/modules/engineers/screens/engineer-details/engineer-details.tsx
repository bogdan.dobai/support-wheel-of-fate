import * as React from 'react';
import {
  View,
  StyleSheet,
  Pressable,
  Alert,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {Colors, Metrics} from '@src/theme';
import {EngineerForm} from '@engineers//components';
import {useEffect, useRef, useState} from 'react';
import {StackScreenProps} from '@react-navigation/stack';
import {
  MainNavigatorRouteProps,
  MainNavigatorRoutes,
} from '@src/navigation/routes';
import {CloseIcon, EditIcon, TrashIcon, UploadIcon} from '@src/assets/icons';
import {ICON_SIZE} from '@src/helpers';
import {useDispatch, useSelector} from 'react-redux';
import {selectCurrentEngineer} from '@engineers/store/selectors';
import {
  addEngineer,
  deleteEngineer,
  editEngineer,
} from '@src/services/firstore';
import {
  AddEngineer,
  DeleteEngineer,
  EditEngineer,
} from '@engineers/store/actions';
import {EngineerClass} from '@engineers/types/classes';
import ImagePicker from 'react-native-image-crop-picker';
import {getImage, uploadImage} from '@src/services/storage';
import {EngineerFormRef} from '@engineers/types/interfaces';
import FastImage from 'react-native-fast-image';
import {isIphone} from '@src/helpers/utils';

const PLACEHOLDER_IMAGE =
  'https://flyinryanhawks.org/wp-content/uploads/2016/08/profile-placeholder.png';

export const EngineerDetails = (
  props: StackScreenProps<
    MainNavigatorRouteProps,
    MainNavigatorRoutes.ENGINEER_DETAILS
  >,
) => {
  const dispatch = useDispatch();
  const engineer = useSelector(selectCurrentEngineer);
  const formRef = useRef<EngineerFormRef>(null);
  const [editMode, setEditMode] = useState<boolean>(
    props.route.params?.editable,
  );
  const [imagePath, setImagePath] = useState<string>(
    getImage(engineer?.id) || PLACEHOLDER_IMAGE,
  );
  const [imageChanged, setImageChanged] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const removeEngineer = async () => {
    await deleteEngineer(engineer?.id);
    dispatch(DeleteEngineer(engineer?.id));
    props.navigation.goBack();
  };

  const onRemoveEngineer = () => {
    Alert.alert(
      'Action required',
      'Are you sure you want to remove this person?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {text: 'OK', onPress: removeEngineer},
      ],
    );
  };

  const renderDeleteButton = () => {
    if (!engineer?.id) return;
    return (
      <Pressable onPress={onRemoveEngineer}>
        <TrashIcon
          style={{marginRight: Metrics.horizontal._12}}
          width={Metrics.horizontal._20}
          height={Metrics.horizontal._20}
          fill={Colors.white}
        />
      </Pressable>
    );
  };

  const onEdit = () => {
    if (editMode) {
      setImagePath(getImage(engineer?.id) || PLACEHOLDER_IMAGE);
      formRef.current.resetData();
    }
    setEditMode(prevState => !prevState);
  };

  const renderHeaderRight = () => {
    return (
      <View style={styles.header}>
        {renderDeleteButton()}
        <Pressable onPress={onEdit}>
          {editMode ? (
            <CloseIcon
              width={ICON_SIZE}
              height={ICON_SIZE}
              fill={Colors.white}
            />
          ) : (
            <EditIcon
              width={ICON_SIZE}
              height={ICON_SIZE}
              fill={Colors.white}
            />
          )}
        </Pressable>
      </View>
    );
  };

  useEffect(() => {
    props.navigation.setOptions({
      headerRight: renderHeaderRight,
    });
  }, [editMode]);

  const onSave = async (
    firstName: string,
    lastName: string,
    department: string,
    position: string,
  ) => {
    if (
      !firstName.length ||
      !lastName.length ||
      !department.length ||
      !position.length
    ) {
      return Alert.alert('', 'All fields are mandatory');
    }
    if (!imagePath.length || (!engineer?.id && !imageChanged)) {
      return Alert.alert('', 'Profile Image is mandatory');
    }
    setLoading(true);
    if (engineer?.id) {
      const response: EngineerClass = await editEngineer(
        engineer.id,
        firstName,
        lastName,
        department,
        position,
      );
      if (imageChanged) {
        await uploadImage(engineer.id, imagePath);
      }
      dispatch(EditEngineer(response));
    } else {
      const response = await addEngineer(
        firstName,
        lastName,
        department,
        position,
      );
      await uploadImage(response.id, imagePath);
      dispatch(AddEngineer(response));
    }
    setLoading(false);
    props.navigation.goBack();
  };

  const onChangeImage = async () => {
    try {
      const image = await ImagePicker.openPicker({
        width: 1000,
        height: 1000,
        cropping: true,
      });
      setImageChanged(true);
      setImagePath(image.path);
    } catch (e) {}
  };

  const renderChangeImageButton = () => {
    if (!editMode) return;
    return (
      <Pressable onPress={onChangeImage} style={styles.changeImageIcon}>
        <UploadIcon
          width={ICON_SIZE}
          height={ICON_SIZE}
          fill={Colors.primary}
        />
      </Pressable>
    );
  };

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        keyboardVerticalOffset={-100}
        behavior={isIphone() ? 'position' : 'height'}
        style={{flex: 1}}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <>
            <View>
              <FastImage style={styles.image} source={{uri: imagePath}} />
              {renderChangeImageButton()}
            </View>
            <EngineerForm
              ref={formRef}
              engineer={engineer}
              onSave={onSave}
              disabled={!editMode}
              loading={loading}
            />
          </>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  image: {
    width: Metrics.screenWidth,
    height: Metrics.screenWidth,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  changeImageIcon: {
    width: Metrics.horizontal._32,
    height: Metrics.horizontal._32,
    borderRadius: Metrics.horizontal._16,
    backgroundColor: Colors.background,
    opacity: 0.7,
    position: 'absolute',
    right: Metrics.horizontal._20,
    bottom: Metrics.horizontal._24,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
