export class EngineerClass {
  id: string;
  firstName: string;
  lastName: string;
  position: string;
  department: string;
  techSkills: string[];
  softSkills: string[];
}
