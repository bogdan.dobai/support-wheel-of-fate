import {ActionType, getType} from 'typesafe-actions';
import * as engineersActions from '@engineers/store/actions';
import {EngineerClass} from '@engineers/types/classes';

export interface EngineersState {
  engineers: Record<string, EngineerClass>;
  selectedEngineer: string;
}

const initialState: EngineersState = {
  engineers: {},
  selectedEngineer: null,
};

export type EngineersActions = ActionType<typeof engineersActions>;

const authReducer = (
  state = initialState,
  action: EngineersActions,
): EngineersState => {
  switch (action.type) {
    case getType(engineersActions.LoadEngineers): {
      const engineers: Record<string, EngineerClass> = {};
      action.payload.forEach((item: EngineerClass) => {
        engineers[item.id] = item;
      });
      return {
        ...state,
        engineers,
      };
    }
    case getType(engineersActions.SelectEngineer): {
      return {
        ...state,
        selectedEngineer: action.payload,
      };
    }
    case getType(engineersActions.AddEngineer):
    case getType(engineersActions.EditEngineer): {
      const engineers: Record<string, EngineerClass> = {...state.engineers};
      engineers[action.payload.id] = action.payload;
      return {
        ...state,
        engineers,
      };
    }
    case getType(engineersActions.DeleteEngineer): {
      const engineers: Record<string, EngineerClass> = {...state.engineers};
      delete engineers[action.payload];
      return {
        ...state,
        engineers,
      };
    }
    default:
      return state;
  }
};

export default authReducer;
