import {createAction} from 'typesafe-actions';
import {EngineerClass} from '@engineers//types/classes';

const LOAD_ENGINEERS = '[Engineers] Load';

const SELECT_ENGINEER = '[Engineers] Select Engineer';

const ADD_ENGINEER = '[Engineers] Add Engineer';
const EDIT_ENGINEER = '[Engineers] Edit Engineer';
const DELETE_ENGINEER = '[Engineers] Delete Engineer';

export const LoadEngineers = createAction(
  LOAD_ENGINEERS,
  (data: EngineerClass[]) => data,
)<EngineerClass[]>();

export const SelectEngineer = createAction(
  SELECT_ENGINEER,
  (id: string) => id,
)<string>();

export const AddEngineer = createAction(
  ADD_ENGINEER,
  (data: EngineerClass) => data,
)<EngineerClass>();

export const EditEngineer = createAction(
  EDIT_ENGINEER,
  (data: EngineerClass) => data,
)<EngineerClass>();

export const DeleteEngineer = createAction(
  DELETE_ENGINEER,
  (id: string) => id,
)<string>();
