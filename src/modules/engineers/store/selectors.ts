import * as Types from '@src/store/state';
import {createSelector} from 'reselect';
import {EngineersState} from '@engineers/store/reducer';
import {EngineerClass} from '@engineers/types/classes';

const getEngineersState = (state: Types.RootState): EngineersState =>
  state.engineers;

export const selectEngineers = createSelector(
  getEngineersState,
  (state: EngineersState): EngineerClass[] => Object.values(state.engineers),
);

export const selectCurrentEngineer = createSelector(
  getEngineersState,
  (state: EngineersState): EngineerClass =>
    state.engineers[state.selectedEngineer],
);
