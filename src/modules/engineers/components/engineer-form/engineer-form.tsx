import * as React from 'react';
import {
  StyleSheet,
  Pressable,
  ScrollView,
  Text,
  ActivityIndicator,
  TextInput,
} from 'react-native';
import {Input} from '@engineers/components';
import {Strings} from '@src/helpers';
import {Colors, Fonts, Metrics} from '@src/theme';
import {ForwardedRef, useImperativeHandle, useRef, useState} from 'react';
import {EngineerClass} from '@engineers/types/classes';
import {EngineerFormRef} from '@engineers/types/interfaces';
import {isIphone} from '@src/helpers/utils';

interface Props {
  engineer: EngineerClass;
  onSave: (
    firstName: string,
    lastName: string,
    department: string,
    position: string,
  ) => void;
  disabled: boolean;
  loading: boolean;
}

export const EngineerForm = React.forwardRef(
  (props: Props, ref: ForwardedRef<EngineerFormRef>) => {
    const lasNameRef = useRef<TextInput>(null);
    const departmentRef = useRef<TextInput>(null);
    const positionRef = useRef<TextInput>(null);
    const [firstName, setFirstName] = useState<string>(
      props.engineer?.firstName,
    );
    const [lastName, setLastName] = useState<string>(props.engineer?.lastName);
    const [department, setDepartment] = useState<string>(
      props.engineer?.department,
    );
    const [position, setPosition] = useState<string>(props.engineer?.position);

    useImperativeHandle(
      ref,
      (): EngineerFormRef => ({
        resetData: () => {
          setFirstName(props.engineer?.firstName);
          setLastName(props.engineer?.lastName);
          setDepartment(props.engineer?.department);
          setPosition(props.engineer?.position);
        },
      }),
    );

    const onSave = () =>
      props.onSave(firstName, lastName, department, position);

    const renderButtonContent = () => {
      return props.loading ? (
        <ActivityIndicator color={Colors.white} />
      ) : (
        <Text style={styles.saveText}>{Strings.SAVE}</Text>
      );
    };

    const renderButton = () => {
      if (props.disabled) return;
      return (
        <Pressable style={styles.saveButton} onPress={onSave}>
          {renderButtonContent()}
        </Pressable>
      );
    };

    return (
      <ScrollView bounces={false} style={styles.content}>
        <Input
          extraStyle={isIphone() ? {} : {marginTop: Metrics.vertical._10}}
          label={Strings.FIRST_NAME}
          value={firstName}
          onChangeText={setFirstName}
          disabled={props.disabled}
          onSubmitEditing={() => lasNameRef?.current?.focus()}
        />
        <Input
          ref={lasNameRef}
          label={Strings.LAST_NAME}
          value={lastName}
          onChangeText={setLastName}
          disabled={props.disabled}
          onSubmitEditing={() => departmentRef?.current?.focus()}
        />
        <Input
          ref={departmentRef}
          label={Strings.DEPARTMENT}
          value={department}
          onChangeText={setDepartment}
          disabled={props.disabled}
          onSubmitEditing={() => positionRef?.current?.focus()}
        />
        <Input
          ref={positionRef}
          label={Strings.POSITION}
          value={position}
          onChangeText={setPosition}
          disabled={props.disabled}
        />
        {renderButton()}
      </ScrollView>
    );
  },
);

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: Metrics.horizontal._24,
    paddingTop: Metrics.vertical._24,
    borderTopLeftRadius: Metrics.horizontal._12,
    borderTopRightRadius: Metrics.horizontal._12,
    position: 'absolute',
    top: Metrics.screenWidth - Metrics.vertical._12,
    width: Metrics.screenWidth,
    backgroundColor: Colors.background,
  },
  saveButton: {
    borderRadius: Metrics.horizontal._12,
    backgroundColor: Colors.accent,
    justifyContent: 'center',
    alignItems: 'center',
    height: Metrics.vertical._44,
  },
  saveText: {
    fontFamily: Fonts.bold,
    fontSize: Metrics.horizontal._16,
    color: Colors.white,
  },
});
