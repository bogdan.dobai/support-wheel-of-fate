import * as React from 'react';
import {View, TextInput, StyleSheet, Text, ViewStyle} from 'react-native';
import {Colors, Metrics} from '@src/theme';
import {Fonts} from '@src/theme/fonts';
import {ForwardedRef} from 'react';

interface Props {
  label: string;
  value: string;
  onChangeText: (value: string) => void;
  disabled?: boolean;
  validator?: () => boolean;
  extraStyle?: ViewStyle;
  onSubmitEditing?: () => void;
}

export const Input = React.forwardRef(
  (props: Props, ref: ForwardedRef<TextInput>) => {
    const onSubmitEditing = () =>
      props.onSubmitEditing && props.onSubmitEditing();

    return (
      <View
        style={[
          styles.container,
          props.extraStyle,
          props.disabled ? styles.disabledContainer : {},
        ]}>
        <View style={styles.labelContainer}>
          <Text style={styles.label}>{props.label}</Text>
        </View>
        <TextInput
          ref={ref}
          style={styles.text}
          value={props.value}
          onChangeText={props.onChangeText}
          editable={!props.disabled}
          blurOnSubmit={false}
          onSubmitEditing={onSubmitEditing}
        />
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderRadius: Metrics.horizontal._12,
    borderWidth: 1,
    borderColor: Colors.primary,
    marginBottom: Metrics.vertical._24,
    height: Metrics.vertical._44,
  },
  disabledContainer: {
    borderColor: Colors.disabled,
  },
  labelContainer: {
    height: Metrics.vertical._24,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -Metrics.vertical._12,
    left: Metrics.vertical._12,
    paddingHorizontal: Metrics.horizontal._8,
    backgroundColor: Colors.background,
  },
  label: {
    fontFamily: Fonts.bold,
    fontSize: Metrics.horizontal._12,
  },
  text: {
    fontFamily: Fonts.regular,
    fontSize: Metrics.horizontal._14,
    height: Metrics.vertical._44,
    marginLeft: Metrics.horizontal._20,
    marginRight: Metrics.horizontal._20,
    color: Colors.primary,
  },
});
