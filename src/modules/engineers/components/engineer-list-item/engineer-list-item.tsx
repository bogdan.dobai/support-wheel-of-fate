import * as React from 'react';
import {Pressable, View, Text, StyleSheet, ViewStyle} from 'react-native';
import {EngineerClass} from '@src/modules/engineers/types/classes';
import {Colors, Metrics} from '@src/theme';
import {ArrowRightIcon} from '@src/assets/icons';
import {ICON_SIZE} from '@src/helpers';
import {getImage} from '@src/services/storage';
import FastImage from 'react-native-fast-image';

interface Props {
  item: EngineerClass;
  onSelectEngineer: (item: EngineerClass) => void;
  onCalendar?: boolean;
  extraStyle?: ViewStyle;
}

export const EngineerListItem = (props: Props) => {
  const onSelectEngineer = () => props.onSelectEngineer(props.item);

  const title = `${props.item.firstName} ${props.item.lastName}`;
  const subtitle = `${props.item.department} - ${props.item.position}`;

  return (
    <Pressable
      style={({pressed}) => [
        styles.container,
        props.extraStyle,
        pressed
          ? {
              transform: [{scale: 1.03}],
            }
          : {},
      ]}
      onPress={onSelectEngineer}>
      <FastImage
        style={styles.image}
        source={{uri: getImage(props.item.id, !props.onCalendar)}}
      />
      <View style={styles.contentContainer}>
        <Text numberOfLines={1} style={styles.name}>
          {title}
        </Text>
        <Text numberOfLines={1} style={styles.department}>
          {subtitle}
        </Text>
        <View style={styles.arrow}>
          <ArrowRightIcon
            fill={Colors.white}
            width={ICON_SIZE}
            height={ICON_SIZE}
          />
        </View>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Metrics.screenWidth - Metrics.horizontal._48,
    borderRadius: Metrics.horizontal._12,
    flexDirection: 'row',
    backgroundColor: Colors.white,
    shadowColor: Colors.primary,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.12,
    shadowRadius: 3.84,
    elevation: 3,
    marginLeft: Metrics.horizontal._24,
  },
  image: {
    height: Metrics.horizontal._84,
    width: Metrics.horizontal._84,
    borderTopLeftRadius: Metrics.horizontal._12,
    borderBottomLeftRadius: Metrics.horizontal._12,
  },
  name: {
    fontSize: Metrics.horizontal._16,
    fontWeight: 'bold',
    paddingBottom: Metrics.vertical._4,
  },
  department: {
    fontSize: Metrics.horizontal._12,
    fontWeight: 'bold',
    color: Colors.disabled,
  },
  contentContainer: {
    paddingLeft: Metrics.horizontal._12,
    paddingRight: Metrics.horizontal._36,
    paddingTop: Metrics.horizontal._16,
    flex: 1,
  },
  arrow: {
    alignSelf: 'flex-end',
    width: Metrics.horizontal._32,
    height: Metrics.horizontal._32,
    borderTopLeftRadius: Metrics.horizontal._12,
    borderBottomRightRadius: Metrics.horizontal._12,
    backgroundColor: Colors.accent,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
  },
});
