import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Colors, Metrics} from '@src/theme';
import {
  Defs,
  LinearGradient as SVGLinearGradient,
  Rect,
  Stop as SVGStop,
  Svg,
} from 'react-native-svg';

export const OverlayHeaderBackground = () => {
  return (
    <Svg style={styles.container}>
      <Defs>
        <SVGLinearGradient id="grad" x1={0} y1={'1'} x2={'0'} y2={'0'}>
          <SVGStop offset="1" stopColor={Colors.primary} stopOpacity="1" />
          <SVGStop offset="0.6" stopColor={Colors.primary} stopOpacity="0.8" />
          <SVGStop offset="0.3" stopColor={Colors.primary} stopOpacity="0.6" />
          <SVGStop offset="0.2" stopColor={Colors.primary} stopOpacity="0.5" />
          <SVGStop offset="0" stopColor={Colors.primary} stopOpacity="0" />
        </SVGLinearGradient>
      </Defs>
      <Rect
        x={0}
        y={0}
        width={Metrics.screenWidth}
        height={'100%'}
        fill="url(#grad)"
      />
    </Svg>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    zIndex: 1,
  },
});
