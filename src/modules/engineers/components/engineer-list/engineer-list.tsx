import * as React from 'react';
import {
  View,
  ActivityIndicator,
  FlatList,
  Text,
  ListRenderItemInfo,
  StyleSheet,
} from 'react-native';
import {EngineerClass} from '@src/modules/engineers/types/classes';
import {EngineerListItem} from '@src/modules/engineers/components';
import {Colors, Metrics} from '@src/theme';
import {Strings} from '@src/helpers';

interface Props {
  data: EngineerClass[];
  loading: boolean;
  onSelectEngineer: (item: EngineerClass) => void;
  onAddEngineers: () => void;
}

export const EngineerList = (props: Props) => {
  const renderItem = ({item}: ListRenderItemInfo<EngineerClass>) => (
    <EngineerListItem item={item} onSelectEngineer={props.onSelectEngineer} />
  );

  const keyExtractor = (item: EngineerClass) => item.id;

  const renderListEmptyComponent = () => {
    if (props.loading) {
      return <ActivityIndicator color={Colors.accent} size={'large'} />;
    }
    return (
      <View style={styles.emptyContainer}>
        <Text>{Strings.NOTHING_TO_SHOW}</Text>
        <Text style={styles.addEngineers} onPress={props.onAddEngineers}>
          {Strings.ADD_ENGINEERS}
        </Text>
      </View>
    );
  };

  const renderItemSeparator = () => {
    return <View style={styles.divider} />;
  };

  return (
    <FlatList
      data={props.data}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      ListEmptyComponent={renderListEmptyComponent}
      ItemSeparatorComponent={renderItemSeparator}
      contentContainerStyle={[
        styles.contentContainer,
        !props.data.length ? styles.emptyContainer : {},
      ]}
      showsVerticalScrollIndicator={false}
    />
  );
};

const styles = StyleSheet.create({
  divider: {
    width: '100%',
    height: Metrics.vertical._16,
  },
  contentContainer: {
    paddingBottom: Metrics.vertical._32,
  },
  emptyContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addEngineers: {
    color: Colors.accent,
    textDecorationLine: 'underline',
    paddingHorizontal: Metrics.horizontal._10,
    paddingVertical: Metrics.vertical._4,
  },
});
