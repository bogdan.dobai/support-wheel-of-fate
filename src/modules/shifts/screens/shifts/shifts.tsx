import {Colors, Metrics} from '@src/theme';
import * as React from 'react';
import {View, StyleSheet} from 'react-native';
import {Agenda} from 'react-native-calendars';
import {EngineerListItem} from '@engineers/components';
import {useDispatch, useSelector} from 'react-redux';
import {selectEngineers} from '@engineers/store/selectors';
import {EngineerClass} from '@engineers/types/classes';
import {useEffect, useState} from 'react';
import moment from 'moment';
import {SelectEngineer} from '@engineers/store/actions';
import {
  HomeTabNavigatorRouteProps,
  HomeTabNavigatorRoutes,
} from '@src/navigation/routes';
import {StackScreenProps} from '@react-navigation/stack';

export const Shifts = (
  props: StackScreenProps<
    HomeTabNavigatorRouteProps,
    HomeTabNavigatorRoutes.SHIFTS
  >,
) => {
  const [items, setItem] = useState({});
  const dispatch = useDispatch();
  const engineers = useSelector(selectEngineers);

  /**
   * Get one random engineer from the list.
   */
  const getRandomPerson = () => {
    return engineers[Math.floor(Math.random() * engineers.length)];
  };

  /**
   * Get one random engineer from the list beside one given.
   *
   * @param firstPerson: The person working on the first shift of the day
   */
  const getSecondRandomPerson = (firstPerson: EngineerClass) => {
    let secondPerson = engineers[Math.floor(Math.random() * engineers.length)];
    while (secondPerson.id === firstPerson.id) {
      secondPerson = engineers[Math.floor(Math.random() * engineers.length)];
    }
    return secondPerson;
  };

  /**
   * Get the first person that meets the conditions (not working in the previous day)
   *
   * @param previousDay Array of 2 engineers working in the previous day
   */
  const getFirstPerson = (previousDay: EngineerClass[]) => {
    let firstHalf = getRandomPerson();
    while (previousDay.findIndex(item => item.id === firstHalf.id) !== -1) {
      firstHalf = getRandomPerson();
    }
    return firstHalf;
  };

  /**
   * Get the second person that meets the conditions.
   *
   * @param previousDay Array of 2 engineers working in the previous day
   * @param firstHalf Person working on the first shift of the day
   */
  const getSecondPerson = (
    previousDay: EngineerClass[],
    firstHalf: EngineerClass,
  ) => {
    let secondHalf = getRandomPerson();
    while (
      previousDay?.findIndex(item => item.id === secondHalf.id) !== -1 ||
      secondHalf.id === firstHalf.id
    ) {
      secondHalf = getRandomPerson();
    }
    return secondHalf;
  };

  /**
   * Function that returns an object with all the dates from now till the end of the year as keys.
   */
  const getDatesInMonth = () => {
    const dates: Record<string, EngineerClass[]> = {};
    const start = moment().startOf('month');
    const end = moment().endOf('year');
    while (start.add(1, 'days').diff(end) < 0) {
      dates[start.format('YYYY-MM-DD')] = [];
    }
    return dates;
  };

  /**
   *
   */
  const addPersonsToDates = (range: Record<string, EngineerClass[]>) => {
    const data = {...range};
    let prevKey = null;
    Object.keys(range).forEach(key => {
      if (!prevKey) {
        const first = getRandomPerson();
        const second = getSecondRandomPerson(first);
        data[key] = [first, second];
      } else {
        const first = getFirstPerson(data[prevKey]);
        const second = getSecondPerson(data[prevKey], first);
        data[key] = [first, second];
      }
      prevKey = key;
    });
    setItem(data);
  };

  const onSelectEngineer = (item: EngineerClass) => {
    dispatch(SelectEngineer(item.id));
    props.navigation.navigate(HomeTabNavigatorRoutes.ENGINEER_DETAILS);
  };

  useEffect(() => {
    const range = getDatesInMonth();
    addPersonsToDates(range);
  }, []);

  return (
    <Agenda
      items={items}
      selected={new Date()}
      minDate={'2021-09-01'}
      maxDate={'2021-12-30'}
      pastScrollRange={50}
      futureScrollRange={50}
      renderItem={(item, firstItemInDay) => {
        return (
          <View
            style={[
              styles.itemContainer,
              firstItemInDay ? styles.firstDayItem : {},
            ]}>
            <EngineerListItem
              item={item}
              onSelectEngineer={onSelectEngineer}
              onCalendar
              extraStyle={styles.itemStyle}
            />
          </View>
        );
      }}
      showClosingKnob
      theme={{
        agendaDayNumColor: Colors.primary,
        agendaDayTextColor: Colors.primary,
        agendaTodayColor: Colors.accent,
        agendaKnobColor: Colors.accent,
      }}
    />
  );
};

const styles = StyleSheet.create({
  firstDayItem: {
    borderTopWidth: 1,
    borderColor: 'rgba(80,80,80,0.3)',
    paddingTop: Metrics.vertical._16,
  },
  itemContainer: {
    marginTop: Metrics.vertical._16,
  },
  itemStyle: {
    width: 'auto',
    marginRight: Metrics.horizontal._24,
  },
});
