module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        root: ['.'],
        alias: {
          '@src': './src',
          '@engineers': './src/modules/engineers',
          '@shifts': './src/modules/shifts',
        },
      },
    ],
  ],
};
